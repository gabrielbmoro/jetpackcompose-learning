package com.example.movieapp.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.movieapp.navigation.screens.MovieScreen
import com.example.movieapp.navigation.screens.details.DetailsScreen
import com.example.movieapp.navigation.screens.home.HomeScreen

@Composable
fun MovieNavigation() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = MovieScreen.HomeScreen.name) {
        composable(MovieScreen.HomeScreen.name) {
            HomeScreen(navController)
        }

        composable(
            route = MovieScreen.DetailsScreen.name.plus("/{movie}"),
            arguments = listOf(
                navArgument(
                    name = "movie",
                ) { type = NavType.StringType }
            )
        ) { backStackEntry ->
            DetailsScreen(
                navController = navController,
                movieId = backStackEntry.arguments?.getString("movie")
                    ?: throw IllegalArgumentException("movie name is required")
            )
        }
    }
}