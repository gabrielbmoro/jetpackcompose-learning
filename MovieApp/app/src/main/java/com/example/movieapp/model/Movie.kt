package com.example.movieapp.model

data class Movie(
    val id: String,
    val title: String,
    val year: String,
    val genre: String,
    val director: String,
    val actors: String,
    val plot: String,
    val poster: String,
    val images: List<String>,
    val rating: String
)

fun getMovies(): List<Movie> {
    return listOf(
        Movie(
            id = "96fb6a18-8e55-11ec-b909-0242ac120002",
            title = "The Avengers",
            year = "2013",
            genre = "science fiction",
            director = "Donald",
            actors = "",
            plot = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            poster = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.yToUqM-74wCTVMyd1Aw6RgHaEK%26pid%3DApi&f=1",
            images = listOf(
                "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpbs.twimg.com%2Fmedia%2FDZ9UzSkVoAAC1HK.jpg%3Alarge&f=1&nofb=1",
                "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages.pushsquare.com%2Faf2496f9abbd0%2F1280x720.jpg&f=1&nofb=1",
                "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwikiany.net%2Fwp-content%2Fuploads%2F2020%2F08%2F5f3e43c5335d4.jpg&f=1&nofb=1",
                "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.IQteumRPDpe0p_qfz1_uywHaEK%26pid%3DApi&f=1"
            ),
            rating = ""
        )
    )
}