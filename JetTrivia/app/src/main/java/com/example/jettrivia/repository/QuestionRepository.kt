package com.example.jettrivia.repository

import android.util.Log
import com.example.jettrivia.data.DataOrException
import com.example.jettrivia.model.QuestionItem
import com.example.jettrivia.network.QuestionAPI
import javax.inject.Inject

class QuestionRepository @Inject constructor(
    private val questionAPI: QuestionAPI
) {

    private val dataOrException = DataOrException<ArrayList<QuestionItem>, Boolean, Exception>()

    suspend fun getAllQuestions(): DataOrException<ArrayList<QuestionItem>, Boolean, Exception> {
        try {
            dataOrException.loading = true
            dataOrException.data = questionAPI.getAllQuestions()
        } catch (e: Exception) {
            dataOrException.e = e
            Log.d("Exc", "getAllQuestions: ${dataOrException.e!!.localizedMessage}")
        } finally {
            dataOrException.loading = false
        }

        return dataOrException
    }

}